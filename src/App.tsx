import React, { useRef, useEffect, useState } from 'react';
import { Object3D } from 'three';
import { Viewer, Shapes } from './creoox-sdk';
interface Props {}

const App = ({}: Props) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const viewerRef = useRef<Viewer>();
  const [shapesList, setShapesList] = useState<Object3D[]>([]);
  const [current, setCurrent] = useState<Object3D | null>(null);
  const [info, setInfo] = useState<{
    type: string;
    position: string;
  } | null>(null);

  // create Viewer
  useEffect(() => {
    viewerRef.current = new Viewer(containerRef.current as HTMLDivElement, {
      updateScene,
      moveShape
    });
    const current = viewerRef.current;
    return () => {
      current.dispose();
    };
  }, []);

  // update scene
  const updateScene = (shapes: Object3D[]) => {
    setShapesList([...shapes]);
  };

  const moveShape = (item: { type: string; position: string }) => {
    setInfo({ ...item });
  };

  // add shapes on viewer
  const addShapes = (name: 'box' | 'sphere' | 'cylinder' | 'torus') => {
    return () => {
      viewerRef.current?.add(new Shapes(name));
    };
  };
  // select shape
  const touchShape = (shape: Object3D) => {
    return () => {
      setCurrent(shape);
      viewerRef.current?.touchShape(shape);
      setInfo(null);
    };
  };

  // unselect shape
  const unTouchShape = (shape: Object3D) => {
    return () => {
      setCurrent(null);
      viewerRef.current?.unTouchShape(shape);
    };
  };

  // select shape
  const deleteShape = (shape: Object3D) => {
    return () => {
      if (current?.uuid === shape.uuid) {
        setCurrent(null);
      }
      viewerRef.current?.deleteShape(shape);
    };
  };

  return (
    <div className="page">
      <div className="sidebar">
        <div className="trunk">Scene:</div>

        {shapesList?.map((item: Object3D) => (
          <div key={item.uuid} className="branches" style={{ color: `#${(item as any).material.color.getHexString()}` }}>
            {item.userData.type}
            <div onClick={current?.uuid !== item.uuid ? touchShape(item) : unTouchShape(item)} className="select-shape">
              {current?.uuid === item.uuid ? (
                <img src="https://cdn-icons-png.flaticon.com/128/386/386527.png" alt="" />
              ) : (
                <img src="https://cdn-icons-png.flaticon.com/128/271/271222.png" alt="" />
              )}
            </div>
            <div onClick={deleteShape(item)} className="unselect-shape">
              <img src="https://cdn-icons-png.flaticon.com/128/3405/3405244.png" alt="" />
            </div>
          </div>
        ))}
      </div>

      <div className="viewer-container" ref={containerRef}>
        <div className="shape-buttons">
          <div className="shape-btn" onClick={addShapes('box')}>
            <img src="https://cdn-icons-png.flaticon.com/128/2985/2985019.png" alt="" />
          </div>
          <div className="shape-btn" onClick={addShapes('sphere')}>
            <img src="https://cdn-icons-png.flaticon.com/128/16/16229.png" alt="" />
          </div>
          <div className="shape-btn" onClick={addShapes('cylinder')}>
            <img src="https://cdn-icons-png.flaticon.com/128/204/204501.png" alt="" />
          </div>
          <div className="shape-btn" onClick={addShapes('torus')}>
            <img src="https://cdn-icons-png.flaticon.com/128/5853/5853764.png" alt="" />
          </div>
        </div>

        {current && (
          <div className="info">
            <div className="type">{info?.type} </div>
            <div className="position">{info?.position}</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default App;
