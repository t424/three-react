import { PerspectiveCamera, Scene, WebGLRenderer, Color, HemisphereLight, Object3D } from 'three';
import { TransformControls } from 'three/examples/jsm/controls/TransformControls';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { Shapes } from './';

interface Props {
  updateScene?: (list: Object3D[]) => void;
  moveShape?: (item: { type: string; position: string }) => void;
}

export default class Viewer {
  /** The scene */
  public scene!: Scene;

  /** The camera */
  public camera!: PerspectiveCamera;

  /** The renderer */
  public renderer!: WebGLRenderer;

  /** The orbit controls */
  public controls!: OrbitControls;

  /** The transform controls */
  public transform!: TransformControls;

  /** container */
  public container: HTMLElement;

  /** Shapes list */
  public shapes: Object3D[] = [];

  public callbacks?: Props;
  public currentShape?: Object3D;

  constructor(container: HTMLElement, callbacks?: Props) {
    this.container = container;
    this.callbacks = callbacks;
    this.init();
    this.animate();
  }

  init() {
    this.camera = new PerspectiveCamera(70, this.container.clientWidth / this.container.clientHeight, 1, 1000);
    this.camera.position.set(7, 7, 7);
    this.camera.lookAt(0, 0, 0);

    this.scene = new Scene();
    this.scene.background = new Color(0xe2e2e2);
    this.renderer = new WebGLRenderer({ antialias: true });
    this.renderer.setSize(this.container.clientWidth, this.container.clientHeight);
    this.container.appendChild(this.renderer.domElement);

    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.enabled = true;
    this.controls.screenSpacePanning = false;
    this.controls.rotateSpeed = -0.5;
    this.controls.enableZoom = true;
    this.controls.target.set(0, 0, 0);
    this.controls.enableDamping = true;
    this.controls.dampingFactor = 0.05;

    this.controls.minDistance = 5;
    this.controls.maxDistance = 20;

    this.transform = new TransformControls(this.camera, this.renderer.domElement);

    this.transform.addEventListener('objectChange', () => {
      if (this.currentShape) {
        this.callbacks?.moveShape?.({
          type: this.currentShape.userData.type,
          position: JSON.stringify(this.currentShape.position)
        });
      }
    });

    this.light();

    window.addEventListener('resize', this.onWindowResized.bind(this), false);
    this.onWindowResized();
  }

  add(shape: Shapes) {
    this.scene.add(shape.object);
    this.shapes.push(shape.object);
    this.callbacks?.updateScene?.(this.shapes);
  }

  light() {
    const hLight = new HemisphereLight(0xffffff, 0x444444);
    hLight.position.set(10, 20, 7);
    this.scene.add(hLight);
  }

  touchShape(shape: Object3D) {
    this.controls.enabled = false;
    this.currentShape = shape;

    this.transform.detach();
    this.transform.attach(shape);
    this.scene.add(this.transform);
  }

  unTouchShape(shape: Object3D) {
    this.controls.enabled = true;
    this.currentShape = undefined;

    this.transform.detach();
  }

  deleteShape(shape: Object3D) {
    if (shape.uuid === this.currentShape?.uuid) {
      this.controls.enabled = true;
      this.transform.detach();
      this.currentShape = undefined;
    }

    shape.parent?.remove(shape);
    this.shapes = this.shapes.filter((item) => item.uuid !== shape.uuid);
    this.callbacks?.updateScene?.(this.shapes);

    // dispose materials and geometry
    shape?.traverse((item: any) => {
      if (item.geometry) {
        item.geometry.dispose();
      }

      if (item.material) {
        if (item.material.length) {
          for (let i = 0; i < item.material.length; ++i) {
            item.material[i].dispose();
          }
        } else {
          item.material.dispose();
        }
      }
    });
  }

  onWindowResized() {
    const width = this.container.clientWidth;
    const height = this.container.clientHeight;
    const ratio = width / height;

    this.camera.aspect = ratio;
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(width, height);
  }

  dispose() {
    this.container.removeChild(this.renderer.domElement);
  }

  animate() {
    requestAnimationFrame(this.animate.bind(this));
    this.controls?.update();
    this.render();
  }

  render() {
    this.renderer.render(this.scene, this.camera);
  }
}
