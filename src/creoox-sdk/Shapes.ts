import { SphereGeometry, BoxGeometry, Mesh, MeshStandardMaterial, CylinderGeometry, Object3D, TorusGeometry } from 'three';

function random(min: number, max: number) {
  let rand = min + Math.random() * (max + 1 - min);
  return rand;
}

function randomColor() {
  const color = Math.floor(Math.random() * 16777215).toString(16);
  return parseInt(`0x${color}`, 16);
}

export default class Shapes {
  public object!: Object3D;

  constructor(name: 'box' | 'sphere' | 'cylinder' | 'torus') {
    this[name]();
  }

  box() {
    const geometry = new BoxGeometry(random(0.8, 1.2), random(0.8, 1.2), random(0.8, 1.2));
    const material = new MeshStandardMaterial({ color: randomColor() });
    this.object = new Mesh(geometry, material);
    this.object.userData.type = 'box';
    this.object.userData.selected = 'false';
  }
  sphere() {
    const geometry = new SphereGeometry(random(0.5, 1.4), 32, 16);
    const material = new MeshStandardMaterial({ color: randomColor() });
    this.object = new Mesh(geometry, material);
    this.object.userData.type = 'sphere';
    this.object.userData.selected = 'false';
  }

  cylinder() {
    const geometry = new CylinderGeometry(random(0.5, 1), random(0.5, 1), 2, 32);
    const material = new MeshStandardMaterial({ color: randomColor() });
    this.object = new Mesh(geometry, material);
    this.object.userData.type = 'cylinder';
    this.object.userData.selected = 'false';
  }
  torus() {
    const geometry = new TorusGeometry(random(0.7, 2), random(0.4, 1), 16, 100);
    const material = new MeshStandardMaterial({ color: randomColor() });
    this.object = new Mesh(geometry, material);
    this.object.userData.type = 'torus';
    this.object.userData.selected = 'false';
  }
}
